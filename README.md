FFMPEG build scripts for Android. Tested on OS X 10.9.4.

## How to to build

You'll have to define `$DIR_NDK` first. DONT use any symbolic links in `$DIR_NDK`.

    $ export DIR_NDK=ffmpeg-stuff/ndk

#### Build for the first time

This will download the sources, generate the toolchain, checkout certain branches, and build everything.

    $ ./build_ffmpeg.sh --init
    $ ./build_ffmpeg_x86.sh --reset
    $ ./build_ffmpeg_arm.sh --reset

#### Build after changes

This will perform a clean build assuming you have the sources and the toolchain.

    $ ./build_ffmpeg_x86.sh
    $ ./build_ffmpeg_arm.sh

## Output

check `$DIR_NDK/bin` for executables.

## To generate test videos

1. Install FFMPEG on your Mac machine: http://www.renevolution.com/how-to-install-ffmpeg-on-mac-os-x/

    $ export PATH=/usr/local/bin:${PATH}
    $ brew update
    $ sudo chown -R ${USER} /usr/local
    $ brew doctor
    $ brew install coreutils yasm libogg libvorbis libvpx
    $ brew link --overwrite yasm libogg libvorbis libvpx
    $ brew remove ffmpeg
    $ brew options ffmpeg
    $ brew install ffmpeg --with-libvorbis --with-libvpx --with-fdk-aac --with-opencore-amr

2. Get a random video:

    $ brew install youtube-dl
    $ youtube-dl -F http://www.youtube.com/watch\?v\=QH2-TGUlwu4
    $ youtube-dl -f 17 http://www.youtube.com/watch\?v\=QH2-TGUlwu4 -o test/video.3gp
    $ ffmpeg -i test/video.3gp

3. Convert to other formats/encodings:

    The script `test/create_videos.sh` goes through all the combinations of video encodings, audio encodings, and formats listed `test/list-video-codecs`, `test/list-audio-codecs`, and `test/list-file-formats`. Be careful when modifying these files thogh. The last element MUST have a newline character at the end.

    $ chmod -R u+x .
    $ ./test/create_videos.sh test/video.3gp

# Extras

## Parallelized Encoder

To speed up encoding, I made a hack script that splits the input video file into chuncks, encode all chuncks concurrently, then combine them into one file. For example, to encode using four chuncks:

$ ./ffmpeg-encode.sh test/video.3gp 4

Note: This is not the way videos should be encoded; Splitting the video into chuncks and encoding each separately makes compression less optimal. It also introduces noticable defects at caoncatenation points depending on codecs used.