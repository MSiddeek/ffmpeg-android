#!/bin/bash

# VERIFY ARGS # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

if [[ -z $1 ]]; then
    echo "Missing input file!"
    echo "    Usage: $0 <video-file> <jobs>"
    exit 1
fi

if [[ -z $2 ]]; then
    echo "Missing number of threads!"
    echo "    Usage: $0 <video-file> <jobs>"
    echo "Assuming 1 thread."
    NUM_THREADS=1
else
    NUM_THREADS=$2
fi

# GET PATHS READY

INPUT_FILE=$(basename $1)
INPUT_PATH=$(dirname $1)
INPUT_NAME=$(echo $INPUT_FILE | cut -d '.' -f1)

OUTPUT_PATH=${INPUT_PATH}/output
LOG=${OUTPUT_PATH}/ffmpeg-encode.log
echo "" > ${LOG}

set -e

rm -rf ${OUTPUT_PATH}
mkdir -p ${OUTPUT_PATH}

# UTILITY FUNCTIONS # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

get_time() {
    echo $(gdate +%s%N)
}

militime_to_hhmmssSSS() {
    militime=$1

    hh=$(($militime / 1000 / 60 / 60))
    militime=$(($militime - $hh * 60 * 60 * 1000))

    mm=$(($militime / 1000 / 60))
    militime=$(($militime - $mm * 60 * 1000))

    ss=$(($militime / 1000))
    militime=$(($militime - $ss * 1000))

    SS=$militime

    printf "%02d:%02d:%02d.%03d" $hh $mm $ss $SS
    return 0
}

hhmmssSSS_to_militime() {
    hh=$(echo "$1" | cut -d ':' -f1)
    mm=$(echo "$1" | cut -d ':' -f2)
    ss=$(echo "$1" | cut -d ':' -f3 | cut -d '.' -f1)
    SS=$(echo "$1" | cut -d '.' -f2)

    printf $(( ((((($hh * 60) + $mm) * 60) + $ss) * 1000) + $SS ))
    return 0
}

# SPLIT AND ENCODE # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

START_TIME=$(get_time)

encode() {
    ffmpeg -i ${INPUT_PATH}/${INPUT_FILE} \
        -ss $1 -to $2 \
        -acodec libvorbis -ac 1 -ar 44100 -ab 62k \
        -vcodec vp8 -r 30 -vb 4096k -pix_fmt yuv420p \
        -y $3 &> $LOG
}

VIDEO_DUR=$(ffmpeg -i ${INPUT_PATH}/${INPUT_FILE} 2>&1 | grep "Duration" \
    | cut -d ' ' -f4 | sed s/,//)
VIDEO_LEN=$(hhmmssSSS_to_militime $VIDEO_DUR)

# Sometimes end of video is thrown away, try to avoid this.
VIDEO_LEN=$(( $VIDEO_LEN + 1000 ))
VIDEO_DUR=$(militime_to_hhmmssSSS $VIDEO_LEN)

CHUNCK_LEN=$(( ($VIDEO_LEN + $NUM_THREADS - 1) / $NUM_THREADS ))

for THREAD_I in $(seq 1 $NUM_THREADS); do
    CHUNCK_END=$(( $THREAD_I * $CHUNCK_LEN ))
    CHUNCK_BEGIN=$(( $CHUNCK_END - $CHUNCK_LEN ))
    FILE_SUFFIX=$(printf "%03d" ${THREAD_I})
    (
        encode $(militime_to_hhmmssSSS $CHUNCK_BEGIN) \
            $(militime_to_hhmmssSSS $CHUNCK_END) \
            ${OUTPUT_PATH}/${INPUT_NAME}_${FILE_SUFFIX}.webm
        echo ${INPUT_NAME}_${FILE_SUFFIX}.webm
    )&
done

# join

for job in `jobs -p`; do
    wait $job
done

# CONCATENATE # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

rm -f ${OUTPUT_PATH}/concat.txt
ORIG_PATH=$(pwd)
cd ${OUTPUT_PATH}
for f in *.webm; do
    echo "file '$f'" >> ./concat.txt
done
cd ${ORIG_PATH}

ffmpeg -f concat -i ./${OUTPUT_PATH}/concat.txt -codec copy \
    -y ${OUTPUT_PATH}/${INPUT_NAME}.webm &> ${LOG}

EXEC_TIME=$(( ($(get_time) - ${START_TIME}) / 1000 / 1000 ))
echo ${EXEC_TIME}ms