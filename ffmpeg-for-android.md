# FFMPEG FOR ANDROID

The ultimate goal of this is to be able to trim common Android video files, scale them, and crop them.

![I HAVE NO IDEA WHAT IM DOING](http://i2.kym-cdn.com/photos/images/original/000/234/765/b7e.jpg)



## FFMPEG OVERVIEW

Looking up available tools for Android (or Linux) that can help:

* [mpgtx](http://mpgtx.sourceforge.net) - Intented for MPEG only.
* [MEncoder](https://help.ubuntu.com/community/MEncoder) - Only transcodes video. No editing.
* [OpenCV](http://docs.opencv.org/doc/tutorials/introduction/android_binary_package/O4A_SDK.html) - Great Android compatability and hardware optimization. But the user has to install [OpenCV Manager](http://docs.opencv.org/platforms/android/service/doc/Intro.html) which is around 20MB.

iOS provides AVAssetExportSession by default which allows encoding/decoding/editing videos. And since Apple has licenses to MPEG formats, it's safe on iOS to do whatever you want with videos. Probably they also have [hardware accelerated encoding](https://developer.apple.com/library/ios/documentation/AudioVideo/Conceptual/MultimediaPG/UsingAudio/UsingAudio.html#//apple_ref/doc/uid/TP40009767-CH2-SW33).

FFmpeg seems to be more viable:

* Covers what we want to do. 
* Allows you to plug in or throw stuff out. (i.e: smaller binary size)
* Widely used and covers many encoders and formats.
* Optimized for some architectures.

### COMPONENTS

When building FFmpeg we are given the option of including/execluding parts of it. Mainly the components we'll have to worry about FFmpeg are:

1. Multiplexers/Demultiplexers - Responsible for reading and writing containers (formats / files). FFmpeg comes with lots of those by default. A demuxer's job is to handle files.
1. Encoders/Decoders (Codecs) - The audio/video decompression/compression algorithms. Those are the ones that decrypt the data stored in containers. Only MPEG codecs are included in FFmpeg by default (you have the option of removing them). To support more encoders and decoders you'll have to get their sources, build them, and point FFmpeg to them.
1. Filters - Which are ways to modify video and audio. In our case: crop, scale, and rotate.
1. Internal libraries - FFmpeg comes with some framework and utility libraries built in (also removable):
    * `libavcodec` - Provides a generic encoding/decoding framework. Not to be confused with the [Libav libavcodec](https://libav.org/).
    * `libavformat` - Provides a generic multiplexing/demultiplexing framework.
    * `libavfilter` - Provides a generic audio/video filtering framework.
    * `libswscale` - Does video scaling and pixel format conversion.
    * `libswresample` - Does audio resampling.
    * `libavutil` - Some utilities.
1. External libraries - FFmpeg supports MPEG codecs by default. To add support for other codecs, we have to build those codecs independently as *external libraries*. Now, when building FFmpeg we have to tell it what external libraries we want to use and where to find them.

### LEGAL ISSUES

    // TODO

#### GPL

    // TODO
* [LGPLv2.1](https://tldrlegal.com/license/gnu-lesser-general-public-license-v2.1-\(lgpl-2.1\))
* [GPLv2](https://tldrlegal.com/license/gnu-general-public-license-v2)
* [GPLv3](https://tldrlegal.com/license/gnu-general-public-license-v3-\(gpl-3\))

#### Patents

    // TODO

### DYNAMIC LINKING VS STATIC LINKING VS EXECUTABLES

    // TODO
    Linking against existent libraries, see /system/lib for Android libraries.

### PACKAGING ANDROID BINARIES

When you build a native executable, you're expected to deliver it with your apk to the user. There are many ways (read: hacks) to do this. Ignoring the ones that require root permissions:

* Make the app download the binary from some file server.

* Store your binaries in `libs/<cpu>/lib_<my-executable>.so` folder. The installer will extract the correct binary in `/data/data/com.yelp.android/lib/lib_<my-executable>.so`, with `+x` permissions. You must stick with the name `lib_<my-executable>.so`. Now, you can simply run:

        Runtime.exe("/data/data/com.yelp.android/lib/lib_<my-executable>.so")

* Put the executables in `assets/<cpu>/<my-executable>`, copy the *correct one* yourself to a data directory, make it executable, and run:

        Runtime.exe("/data/data/com.yelp.android/<my-executable>")

I picked the last one as it seems simpler and safer. By safer I mean one on which we have total control.



### FFMPEG ON MAC

You might want to install FFmpeg on your development machine to do some experimenting, or generate test videos for your FFmpeg Android build.

**However**, I wouldn't install FFmpeg or any of it's dependencies untill I can build it successfully for Android first. When you're trying to build FFmpeg for Android, you don't want to confuse your dev machine stuff with your Android platform stuff. It'll will be easier if you get "some library is missing", than to get a "successful build" although it's actually a build that compiled with your Mac `/usr/bin/gcc`.

#### Install through Homebrew

When install FFmpeg on your Mac machine through hombebrew, you can let homebrew take care of dependencies, or you can to install them yourself:

    $ export PATH=/usr/local/bin:${PATH}
    $ brew update
    $ sudo chown -R ${USER} /usr/local
    $ brew doctor
    $ brew install coreutils yasm libogg libvorbis libvpx

Then perform a clean install of FFmpeg:

    $ brew link --overwrite yasm libogg libvorbis libvpx
    $ brew remove ffmpeg
    $ brew options ffmpeg
    $ brew install ffmpeg --with-libvorbis --with-libvpx --with-fdk-aac --with-opencore-amr
This installation should cover all the supported Android video and audio encoding algorithms.

#### Get YouTube videos

    $ brew install youtube-dl
    $ youtube-dl -F http://www.youtube.com/watch?v=QH2-TGUlwu4
    $ youtube-dl -f 17 http://www.youtube.com/watch?v=QH2-TGUlwu4 -o test/video.3gp
    $ ffmpeg -i test/video.3gp

#### Generate test videos

I put together this `create-videos.sh` script that takes an input video and encodes it using all combinations of video encodings, audio encodings, and formats. You have to list those in `list-video-codecs`, `list-audio-codecs`, and `list-file-formats`.

    $ chmod -R u+x .
    $ ./create_videos.sh video.3gp

The script generates logs to `create-videos.log`.

### Useful FFmpeg commands

* `ffmpeg -encoders`, `ffmpeg -decoders`, `ffmpeg -formats` show you the encoders, decoders, and containers supported by your FFmpeg build. `ffmpeg -codecs` is not as helpful. It lists everything (whether supported or not) and prints flags to indicate what functionality the build supports regarding those codecs.

* If you want to display simple metadata (method of encoding, rates, bitrates...) about some video, do:

        $ ffmpeg -i <video-file>



## HOW TO BUILD ANDROID NATIVE CODE

First, you'll have to get the Android NDK and generate a toolchain through `<ndk-path>/build/tools/make-standalone-toolchain.sh`. The toolchain is an Android environment you can have on your development machine. So, if you run:

    $ <ndk-path>/build/tools/make-standalone-toolchain.sh \
        --platform=android-9 \
        --toolchain=arm-linux-androideabi \
        --install-dir=<ndk-path>/platforms/android-9/arch-arm/usr \
        --stl=stlport
you get the toolchain at `<ndk-path>/platforms/android-9/arch-arm/usr`, which a small linux system that looks something like this:

    <ndk-path>/platforms/android-9/arch-arm/usr$ brew install tree
    <ndk-path>/platforms/android-9/arch-arm/usr$ tree -L 3

    └── usr
        ├── bin
        │   ├── arm-linux-androideabi-ar
        │   ├── arm-linux-androideabi-as
        │   ├── arm-linux-androideabi-g++
        │   ├── arm-linux-androideabi-gcc
        │   ├── arm-linux-androideabi-ld
        │   ├── arm-linux-androideabi-nm
        │   ├── arm-linux-androideabi-ranlib
        │   ├── arm-linux-androideabi-strip
        │   └── ...
        ├── include
        │   ├── malloc.h
        │   ├── math.h
        │   ├── pthread.h
        │   ├── stdio.h
        │   ├── stdlib.h
        │   ├── string.h
        │   └── ...
        ├── lib
        │   ├── libstdc++.a
        │   ├── libGLESv2.so
        │   ├── pkgconfig
        │   └── ...
        └── ...

So, if you want to build a makefile or an autoconf project, you'll have to use the toolchain for that. You can place the project source codes any place you want, as long as you have permissions there. The important parts of the toolchain are:

* `bin` has the assemblers, compilers, and linkers you want.
* `include` has your headers.
* `lib` has your libraries.

Makefile projects are built by doing `./configure`, `make`, then `make install`. Autoconf projects are built by doing `./autogen.sh`, `./configure` (not always), `make`, then `make install`.

* `./configure` and `autogen.sh` generate Makefiles and build scripts with the appropriate compilers/linkers. `autogen.sh` is smarter. It tests the compilers in your toolchain to gather information about them (e.g. how big an integer is in your `arm-linux-androideabi-gcc` compiler). This helps it optimize the building process.
* `make` builds the source code and outputs everything in the project's local directory.
* `make install` installs the binaries, headers, and libraries on your target platform.


As simple as this sounds in theory, it's not always as simple in practice. When cross-compiling things get a bit tricky. Makefile/autotools projects do not always `./configure` or `./autoconf` in a standard way. Autoconf projects are easier to build in case you have a recent source and it supports Android already. Otherwise, you'll have to figure out how to the build process for this specific project goes.

### BUILDING STEPS

The steps vary between different projects, but the general flow goes:

1. `./configure` or `./autoconf && ./configure` - with the appropriate arguments / environment-variables. To know what arguments you have to provide, do `./configure --help` or `./autogen.sh --help` and read the `README` carefully. 
Here's a list of common ways to define those arguments:

    * System root - The place where the build system can find what it needs to build the project. Which should be your Android toolchain path. For example `--sysroot=<android-toolchain>` or `SYSROOT=<android-toolchain>`.

    * Compiler paths:
        * Sometimes they ask for prefixes like `--host=arm-linux-androideabi`.
        * Or for the path like `export PATH=${PATH}:<android-toolchain>/bin`.
        * Or path prefixes as in `export CROSS=<android-toolchain>/bin/arm-linux-androideabi-`.
        * Or even for the compiler binary itself `export CC=<android-toolchain>/bin/arm-linux-androideabi-gcc`.
        
    * Compiler flags - Which should at least include your toolchain libraries/headers.
        * Through arguments: `--extra-cflags=-I<android-toolchain>/include` or `--extra-ldflags=-L<android-toolchain>/lib`.
        * Through environment variables: `export CPPFLAGS="-I<android-toolchain>/include"` or `export LDFLAGS="-L<android-toolchain>/lib"`.

    * Install directory - The place where `make install` will install stuff. Normally, should be your toolchain root too. For example: `--prefix=<android-toolchain>`.

    * Host / Target OS - The OS you want to build for. As in `--target-os=linux`.

    * Enable cross compile - Enable this flag if it's there.

    * SDK path - Available mostly for autoconf tools that recognize Android. Should be the path to your NDK.

1. `./make` - which does the actual building.

    Not all problems with running `./configure` are obvious during the previous step. The `./configure` script can return successfully (most of the time it does). But you can only see that you made a mistake with `./configure` after you run `./make`. Sometimes problems don't even show up until you do the next step. Normally, the only argument you need to give to this is the number of threads you want involved.

1. `./make install` - moves built binaries, libraries, and their corresponding headers to your install directory.

    As mentioned you should tell `./configure` to install stuff to your toolchain. Now, if later you want to build some other project `Y` that depends on project `X` you just built, `Y`'s build script 'd better be able to find the required `X` libraries and headers in your toolchain.

    Unless you're doing something special, at which point you may want to make the installation of `X` happen in a different directory. But make sure to tell `Y` where to find the installation `X`. E.g. add more paths to `--extra-cflags` and `--extra-ldflags`.

### TIPS ON CROSS-COMPILATION

Random tips:

* Read the `README`, `./configure --help`, and `./autogen.sh --help` very carefully. Make sure to cover all the arguments/environment-variables. You don't want the build system to make assumptions about your target machine and give you misleading success codes for everything.
* The examples given above for arguments and environment variables are just that, examples. DO NOT define all of them, only the ones required.
* I don't know why this is a thing, but avoid symbolic links at all costs.
* Don't `sudo` anything. If you get to a point where you have to that, you're probably doing something worng.
* Don't modify bash scripts during their execution.
* When something won't build (this will happen);
    * Don't try random fixes. Just DONT!
    * Go to a clean terminal (one where you haven't defined new environment variables), and try to reproduce the same error. Sometimes leftover environment variables may break the build.
    * Make sure that the error message you get explains the actual error not a symptom of a bigger problem.
    * Look for configuration logs (e.g. `config.log`). They explain in extensive detail how the configuration went:
        * Make sure the paths you provided to the configure scripts are actually being used.
        * Look out for things that seem to be using your development machine toolchain.
        * Look out for missing compilers, linkers, ranlibs, strips...
        * Disable docs, examples, and tests.
* The [FFmpeg irc channel](http://irc.lc/freenode/ffmpeg/t4nk@@@) has people who are good with nitty gritty details of video and audio encoding in general. But not necessarily with building FFmpeg, espicially for Android. But they may help with troubleshooting general problems with FFmpeg.
* Assuming you successfully built some project, know the release / commit-hash that you built the project on. If your script works on some release, it may not work on the next.


## BUILDING FFMPEG FOR ANDROID

So, for our purposes, we want to be able to input the common Android video formats/encodings, do some trimming and scaling, then output them in a format that Android can understand. A list of Android supported formats can be found here:

    http://developer.android.com/guide/appendix/media-formats.html

| Containter | Patent    | In FFmpeg                   | Notes | Extension 
|:----------:|:--------- |:--------------------------- |:----- |:---------
| 3GPP       | Patented  | `--enable-demuxer=mov`      | | `.3gp`
| MPEG-4     | Patented  | `--enable-demuxer=mov`      | [MPEG LA license](http://www.mpegla.com/main/programs/M4V/Pages/Intro.aspx) | `.mp4`    
| MPEG-TS    | Patented  | `--enable-demuxer=mov`      | [MPEG LA license](http://www.mpegla.com/main/programs/m2s/Pages/Intro.aspx) | `.ts`     
| Matroska   | No patent | `--enable-demuxer=matroska` | It should support all audio and video codecs. *Decodable (playable) on Android 4.0+*. [Matroska-legal](http://www.matroska.org/info/legal/index.html) | `.mkv`    
| **WebM**   | No patent | `--enable-demuxer=webm`     | Sponsored by Google and based on Matroska. Intented to be used with Vorbis (audio codec) and VPX (video codec) only. *Decodable for Android 2.3.3+*. [WemM project license](http://www.webmproject.org/license/bitstream/) [Webm repo](https://github.com/webmproject/libwebm/) | `.webm`   

| Video codedc  | Patent     | In FFmpeg                   | Notes | Containers*
|:-------------:|:---------- |:--------------------------- |:----- |:----------
| H.263         | Patented   | `--enable-decoder=h263`  |       | `3GP` `Matroska` `MP4` `MPEG-TS`
| H.264         | Patented   | `--enable-decoder=h264`  | [MPEG LA license](http://www.mpegla.com/main/programs/AVC/Pages/Intro.aspx) | `3GP` `Matroska` `MP4` `MPEG-TS`
| MPEG-4 SP     | Patented   | `--enable-decoder=mpeg4` |       | `Matroska` `MP4` `MPEG-TS`
| **VP8**       | No patent  | `--enable-libvpx` `--enable-decoder=libvpx_vp8` | [libvpx patent](https://github.com/webmproject/libvpx/blob/master/PATENTS) | `Matroska` `MP4` `WebM`

| Audio codedc                       | Patent     | In FFmpeg | Notes     | Contatiners*
|:----------------------------------:|:---------- |:--------- |:--------- |:------------
| AAC LC                             | Patented   | `--enable-decoder=aac` or `--enable-libfaac` or `--enable-libfdk_aac`  or `--enable-libvo_aacenc` | `libfaac`, `libfdk_aac` are not compatible with GPL. [fraunhofer](http://www.iis.fraunhofer.de/en/ff/amm/prod/audiocodec/audiocodecs/aaclc.html) [AAC](http://www.vialicensing.com/licensing/aac-overview.aspx) [AAC License FAQ](http://www.vialicensing.com/licensing/aac-faq.aspx) | `3GP` `Matroska` `MP4` `MPEG-TS`
| HE-AACv1, HE-AACv2 (enhanced AAC+) | Patented   | `--enable-libaacplus` or `--enable-libfdk_aac` | `libaacplus`, `libfdk_aac` are not compatible with GPL.  [fraunhofer](http://www.iis.fraunhofer.de/en/ff/amm/prod/audiocodec/audiocodecs/heaac.html) [AAC](http://www.vialicensing.com/licensing/aac-overview.aspx) [AAC License FAQ](http://www.vialicensing.com/licensing/aac-faq.aspx) | `3GP` `Matroska` `MP4` `MPEG-TS`
| AAC ELD (enhanced low delay AAC)   | Patented   | `--enable-libfdk_aac` | [fraunhofer](http://www.iis.fraunhofer.de/en/ff/amm/prod/audiocodec/audiocodecs/aaceld.html) | `3GP` `Matroska` `MP4` `MPEG-TS`
| AMR-NB, AMR-WB                     | No patent  | `--enable-decoder=amrnb` `--enable-decoder=amrwb` | OpenCORE is under Apache2 which is incompatible with LGPL2. FFmpeg should be under LGPL3 to be able to use OpenCORE. [OpenCORE license](https://github.com/artclarke/xuggle-xuggler/blob/master/captive/libopencore-amr/csrc/LICENSE) [FFmpeg page on OpenCORE](https://www.ffmpeg.org/general.html#OpenCORE_002c-VisualOn_002c-and-Fraunhofer-libraries) | `3GP` `Matroska`
| FLAC                               | No patent  | `--enable-decoder=flac` | Lossless. [FLAC license](https://xiph.org/flac/license.html) | `Matroska`
| MP3                                | No patent  | `--enable-decoder=mp3` | [libLAME license](http://lame.sourceforge.net/license.txt) | `Matroska` `MP4` `MPEG-TS` 
| MIDI                               |            | | Irrelevant; music synthesis. |
| PCM/WAVE                           |            | | Used in audio-only containers. [Wikipedia](http://en.wikipedia.org/wiki/Pulse-code_modulation) |
| **Vorbis**                         | No patent  | `--enable-libvorbis` `--enable-decoder=libvorbis` |[Vorbis FAQ](http://www.vorbis.com/faq/#com) [xiph.org](http://xiph.org/vorbis/)  | `Matroska` `MP4` `WebM`

[*] Only Android supported containters are listed.

So, the choice that we'll go with is a WebM container with VP8 for video and Vorbis for audio.

1. No patents/licensing required.
1. Brightcove accepts WebM files.
1. We'll have to support decoding WebM anyways, so binary size overhead won't be significant.
1. Our ouput videos will be playable for Android 2.3.3+.
1. Those are not experimental codecs. Lots of codecs in FFmpeg are experimental and are not as efficient as the non-experimental ones.

Relevant pages:

    http://en.wikipedia.org/wiki/Comparison_of_container_formats
    https://trac.ffmpeg.org/wiki/Encode/VP8
    https://trac.ffmpeg.org/wiki/Encode/AAC
    https://trac.ffmpeg.org/wiki/TheoraVorbisEncodingGuide

### BUILDING EXTERNAL LIBRARIES

While building FFmpeg itself is somewhat straight forward, it's external libraries (dependencies) that may require some digging. To know what dependencies you need and in which order you have to build them, you have a couple of options:

* Look up the libraries you want to build and see what dependencies they require. For WebM:

        https://sites.google.com/a/webmproject.org/wiki/ffmpeg/building-with-libvpx
    But it's not always that easy to find clear references.
* This FFmpeg page tells you what you need to build FFmpeg for Ubuntu to get `libass`, `libfdk`, `libtheora`, `libvorbis`, `libmp3lame`, `libopus`, and `libvpx`:

        https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu
* A trick would be to use Homebrew to know dependencies. If you do:

        $ brew uninstall -f libass frei0r libcaca libquvi libvo-aacenc \
            openjpeg opus schroedinger theora xvid libogg ibvpx faac \
            fdk-aac lame opencore-amr yasm ffmpeg
        $ brew options ffmpeg
    Look at the list and find what options you'd like FFmpeg built with, then install FFmpeg on your Mac with those. For example:

        $ brew install ffmpeg --with-libvorbis --with-libvpx \
            --with-opencore-amr --with-fdk-aac --HEAD
    The install will tell you what dependenceis you need on the first line. In this case:

        ==> Installing dependencies for ffmpeg:
            yasm, faac, lame, xvid, libogg, libvpx, opencore-amr, fdk-aac
    Those are given in order too. It'll also give you the configure script at the end:

        $ ./configure --prefix=/usr/local/Cellar/ffmpeg/HEAD --enable-shared
            --enable-pthreads --enable-gpl --enable-version3 --enable-nonfree
            --enable-hardcoded-tables --enable-avresample --enable-vda
            --cc=clang --host-cflags= --host-ldflags= --enable-libx264
            --enable-libfaac --enable-libmp3lame --enable-libxvid
            --enable-libvorbis --enable-libvpx --enable-libopencore-amrnb
            --enable-libopencore-amrwb --enable-libfdk-aac

Follows is brief how-to's on building some libraries for Android ARM CPUs. Mainly we're trying to build FFmpeg to include `libvpx` and `libvorbis`:

    http://wiki.webmproject.org/ffmpeg/building-with-libvpx

#### Yasm

Yasm does assembeler optimisation. It should make the FFmpeg build quicker when processing x264. Yasm from `git://github.com/yasm/yasm.git` comes with a recent `./autogen.sh` which recognizes Android. So, things are quite simple:

    $ ./autogen.sh --host=arm-linux-androideabi \
        --prefix=<ndk-dir>/platforms/android-9/arch-arm/usr
    $ make
    $ make install

#### Ogg

We don't need Ogg but Vorbis requires it as a dependency. You can get Ogg from `git://git.xiph.org/mirrors/ogg.git`. Trying to do the same with Ogg as we did with Yasm doesn't work. Looking at the logs, turns out the build system cant find the compiler. So, we add the toolchain binaries directory to `PATH`. Trying to build with binaries exposed also doesn't work. Looking through the logs again, turns out the build system is using the dev machine `ranlib`, not the toolchain one. So, defining `RANLIB` seems to do the trick.

    $ export PATH=${PATH}:<ndk-dir>/platforms/android-9/arch-arm/usr/bin
    $ export RANLIB=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/rm-linux-androideabi-ranlib
    $ ./autogen.sh --host=arm-linux-androideabi \
        --prefix=<ndk-dir>/platforms/android-9/arch-arm/usr \
        --with-sysroot=<ndk-dir>/platforms/android-9/arch-arm/usr \
        --disable-shared
    $ make
    $ make install

#### Vorbis

Vorbis depends on Ogg. You can find out about that from the `README`. The source from `http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.4.tar.gz` doesn't have `./autogen.sh`. So we'll try to go with `./configure` with `--prefix`, `--with-sysroot`, and  `--host` options. This seems to work!

But when we actually try to build FFmpeg to include `libvorbis`, it gives an error message `Cannot find libvorbis`, although you can find the `libvorbis.a` in `lib`. Looking through the `libvorbis` config logs, turns out the build system is using the wrong compiler. Defining individual toolchain binary path explicitly through environment variables gets it working.

    $ export CC=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-gcc
    $ export CXX=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-g++
    $ export LD=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-ld
    $ export STRIP=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-strip
    $ export NM=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-nm
    $ export AR=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-ar
    $ export AS=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-as
    $ export RANLIB=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-ranlib    
    $ ./configure --prefix=<ndk-dir>/platforms/android-9/arch-arm/usr \
        --with-sysroot=<ndk-dir>/platforms/android-9/arch-arm/usr \
        --host=arm-linux-androideabi --disable-shared
    $ make
    $ make install

#### Vpx

You can get `libvpx` from `http://git.chromium.org/webm/libvpx.git`. `libvpx` configures differently. Doing `./configure --help`, we need to specify `--target`, and `--sdk-path`. Doing so, it doesn't work. Looking at the end of `README`, it says we need to define `CROSS` environment variable.

    $ export CROSS=<ndk-dir>/platforms/android-9/arch-arm/usr/bin/arm-linux-androideabi-
    $ ./configure --target=armv7-android-gcc --sdk-path=<ndk-dir> \
        --prefix=<ndk-dir>/platforms/android-9/arch-arm/usr --disable-vp9 \
        --disable-examples --disable-runtime-cpu-detect --disable-realtime-only \
        --enable-vp8-encoder --enable-vp8-decoder
    $ make
    $ make install

### FFmpeg

You can find many tutorials on building FFmpeg for ARM Androids. Most of them won't work because they mostly target building FFmpeg without any external lbraries. You have to tell `./configure` where to find external libraries and it should work smoothly.

    $ export PATH=${PATH}:<ndk-dir>/platforms/android-9/arch-arm/usr/bin
    $ ./configure \
        --prefix=<ndk-dir>/platforms/android-9/arch-arm/usr --arch=arm --target-os=linux \
        --extra-ldflags="-L<ndk-dir>/platforms/android-9/arch-arm/usr/lib" \
        --extra-cflags="-I<ndk-dir>/platforms/android-9/arch-arm/usr/include" \
        --extra-cxxflags="-I<ndk-dir>/platforms/android-9/arch-arm/usr/include" \
        --enable-cross-compile --cross-prefix=arm-linux-androideabi- \
        --sysroot=<ndk-dir>/platforms/android-9/arch-arm/usr \
        ...
    $ make
    $ make install
Sometimes you won't realize that one of the dependencies was not built correctly until you try build FFmpeg. If this happens, you'll have to go back to that external library and try to figure out what went wrong. Refer to the tips above on how to identify and troubleshoot problems.



## NOTES ON FFMPEG ENCODING

### FASTER ENCODING

Here are some ideas on making the encoding quicker.

#### Encode concurrently

One option is concurrency. Instead of encoding the whole video, we can split it, encode both halves concurrently, then join them back. Obviously, we would want to encode both halves using the same method. The trick here is to choose a way for concatenation that doesn't introduce too much overhead:

* File level concatenation - some MPEG containers provide this. It's extremely quick:

        $ ffmpeg -i "concat:vid1.mp4|vid2.mp4|vid3.mp4" -c copy vid.mp4

* `concat` demuxer - for other containers, we can use the `concat` demuxer. It takes in a text file with a list of video file paths and concatenates them.

        $ cat videolist.txt
            vid1.webm
            vid2.webm
            vid3.webm
        $ ffmpeg -f concat -i videolist.txt -c copy vid.webm

These methods may *NOT* work properly if you try to join non-equal length files.

Be careful though, this is not the correct way to do encoding. Splitting the video disallows the encoder from utilizing the whole video information to do a better encoding (smaller file) in general. Also, at the join points, you might notice small problems with audio or video depending on the encoders you use.

If you're going to do the concurrency with Java, here are some tips:

* You may want to consume your process ouput. Asyncronously of course. See: [Process Javadoc](http://docs.oracle.com/javase/7/docs/api/java/lang/Process.html):

    > ...Because some native platforms only provide limited buffer size for standard input and output streams, failure to promptly write the input stream or read the output stream of the subprocess may cause the subprocess to block, or even deadlock.

#### Don't encode

One option, in case we want to do **trimming only**, would not require us to do any encoding. The way this works is by simply dropping the parts of the video file that we don't need. Although this is very fast and would make the binary size small, it has downsides:

1. You can only trim videos at keyframes. So, if you have a keyframe at 10:00 and you want to trim starting at 9:00, you will get a black screen for the first second (9:00 - 10:00). Also, keyframes are unpredictable, because whoever encoded the video has total control over *how many keyframes* there are.
1. We won't be able to do scaling or cropping.
1. This way is not guaranteed to work with all containers.



## ANALYSIS

    (╯°□°）╯︵ bǝdɯɟɟ

We generate some test videos using FFmpeg on Mac through `./create-videos.sh ./vid.mp4`. Couldn't add AMR to the audio codecs, so `amr_nb` and `amr_wb` are left out of this listing:

    .3gp
                    faac    aac_he   aac_he2   aac_eld      flac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |    6123kb    6140kb    6140kb    6142kb         X         X         X         X
       h263p |         X         X         X         X         X         X         X         X
       mpeg4 |    6180kb    6197kb    6198kb    6200kb         X         X         X         X
         vp8 |         X         X         X         X         X         X         X         X

    .mp4
                    faac    aac_he   aac_he2   aac_eld      flac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |    6123kb    6140kb    6141kb    6143kb         X    6144kb         X    6122kb
       h263p |         X         X         X         X         X         X         X         X
       mpeg4 |    6180kb    6197kb    6198kb    6200kb         X    6201kb         X    6180kb
         vp8 |         X         X         X         X         X         X         X         X

    .ts
                    faac    aac_he   aac_he2   aac_eld      flac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |    6635kb    6654kb    6654kb         X    7559kb    6654kb    7155kb    6630kb
       h263p |    6625kb    6644kb    6644kb         X    7549kb    6644kb    7146kb    6620kb
       mpeg4 |    6695kb    6713kb    6713kb         X    7618kb    6714kb    7215kb    6689kb
         vp8 |    6783kb    6802kb    6801kb         X    7707kb    6802kb    7304kb    6777kb

    .webm
                    faac    aac_he   aac_he2   aac_eld      flac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |         X         X         X         X         X         X         X         X
       h263p |         X         X         X         X         X         X         X         X
       mpeg4 |         X         X         X         X         X         X         X         X
         vp8 |         X         X         X         X         X         X         X    6254kb

    .mkv
                    faac    aac_he   aac_he2   aac_eld      flac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |    6117kb    6136kb    6136kb    6137kb    6965kb    6139kb    6565kb    6118kb
       h263p |    6106kb    6125kb    6125kb    6126kb    6954kb    6128kb    6554kb    6107kb
       mpeg4 |    6177kb    6196kb    6196kb    6197kb    7025kb    6199kb    6625kb    6178kb
         vp8 |    6254kb    6273kb    6273kb    6274kb    7102kb    6276kb    6702kb    6255kb

    X - Couldn't encode, probably incompatability issues.

To test which of these videos actually work on Android, we use a function to determine this:

    static boolean isAndroidSupported(File in) {
        try {
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(in.getAbsolutePath());
            player.prepare();
            player.pause();
            player.stop();
            return true;
        } catch (Exception e) {
        }
        return false;
    }

To test which of those FFmpeg supports, we encode 10ms of the video:

    $ ffmpeg -i <video-file> -ss 0 -to 10 <some-encoding-parameters> <output-video>.webm

The encoding parameters being:

    Container: WebM
    Audio encoder: Vorbis
        Audio rate: 44100
        Audio bitrate: 62kb
    Video encoder: VP8
        Video fps: 30
        Video bitrate: 4096
        Pixel format: yuv240p
        Dimensions: 720x720 or less

This is not ideal, but this is the way we handle it in the app to tell before-hand whether a file is decodeable. On a Nexus 5, we get this:

    .3gp
                     aac    aac_he   aac_he2   aac_eld       lac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |        AB        AB        AB        A.        --        --        --        --
       h263p |        --        --        --        --        --        --        --        --
       mpeg4 |        AB        AB        AB        A.        --        --        --        --
         vp8 |        --        --        --        --        --        --        --        --

    .mp4
                     aac    aac_he   aac_he2   aac_eld       lac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |        AB        AB        AB        A.        --        .B        --        .B
       h263p |        --        --        --        --        --        --        --        --
       mpeg4 |        AB        AB        AB        A.        --        .B        --        .B
         vp8 |        --        --        --        --        --        --        --        --

    .ts
                     aac    aac_he   aac_he2   aac_eld       lac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |        AB        AB        AB        --        AB        AB        AB        AB
       h263p |        AB        AB        AB        --        ..        AB        ..        ..
       mpeg4 |        AB        AB        AB        --        AB        AB        AB        AB
         vp8 |        AB        AB        AB        --        ..        AB        ..        ..
 
    .webm
                     aac    aac_he   aac_he2   aac_eld       lac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |        --        --        --        --        --        --        --        --
       h263p |        --        --        --        --        --        --        --        --
       mpeg4 |        --        --        --        --        --        --        --        --
         vp8 |        --        --        --        --        --        --        --        AB

    .mkv
                     aac    aac_he   aac_he2   aac_eld       lac       mp3       wav    vorbis
                --------  --------  --------  --------  --------  --------  --------  --------
        h264 |        AB        AB        AB        A.        AB        AB        AB        AB
       h263p |        AB        AB        AB        A.        .B        AB        .B        AB
       mpeg4 |        AB        AB        AB        A.        AB        AB        AB        AB
         vp8 |        AB        AB        AB        A.        AB        AB        AB        AB

    A: Android supports it.
    B: Our build supports it.

So, we support all the videos that Android supports except for ones with `aac_eld` audio.

### DEVICE SUPPORT

Our FFmpeg build successfully encodes videos on:

* Nexus 5 (ARM).
* Genymotion Nexus 5 (x86).
* Nexus S.
* Nexus 10.

