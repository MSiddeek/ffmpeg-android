#!/bin/bash

if [ "$1" = "" ]; then
    printf "Missing input file!\n"
    exit 1
fi

# PATHS AND LOGS

INPUT_FILE=$1
OUTPUT_PATH=./output
rm -rf ${OUTPUT_PATH} >> /dev/null 2>&1
mkdir -p ${OUTPUT_PATH}

LOG="create-videos.log"
echo "" > $LOG 2>&1

INCOMPATIBLE="create-videos.incompatible"
echo "" > $INCOMPATIBLE 2>&1

# UTILITIES

get_time() {
    echo $(gdate +%s%N)
}

acodec_to_filename() {
    sed "s/lib//g" <<< \
        $(sed "s/_v//g" <<< \
            $(sed "s/lame//g" <<< \
                $(sed "s/adpcm_ima_//g" <<< \
                    $(sed "s/libfdk_aac -profile:a //g" <<< \
                        $@))))
}

# ENCODING FUNCTION

create_video() {
    ARG_FORMAT=$1
    ARG_VCODEC=$2
    ARG_ACODEC=$3
    # ENCODE
    OUTPUT_FILE=${OUTPUT_PATH}/${ARG_VCODEC}__$(acodec_to_filename ${ARG_ACODEC})${ARG_FORMAT}
    START_TIME=$(get_time)

    echo "" \
        >> $LOG 2>&1
    echo ffmpeg -i ${INPUT_FILE} \
        -acodec $ARG_ACODEC -ac 2 -ar 44100 -ab 62k \
        -vcodec $ARG_VCODEC -r 30 -vb 4096k -pix_fmt yuv420p \
        -y -loglevel warning ${OUTPUT_FILE} \
        >> $LOG 2>&1

    ffmpeg -i ${INPUT_FILE} \
        -acodec $ARG_ACODEC -ac 2 -ar 44100 -ab 62k \
        -vcodec $ARG_VCODEC -r 30 -vb 4096k -pix_fmt yuv420p \
        -y -loglevel warning ${OUTPUT_FILE}\
        >> $LOG 2>&1

    # CHECK FAILURE
    if ! [ $? -eq 0 ]; then
        echo $OUTPUT_FILE >> $INCOMPATIBLE
        rm ${OUTPUT_FILE} >> /dev/null 2>&1
        printf "%10s" "X"
        printf "    %8s | %8sms | %s\n" "X" "X" ${OUTPUT_FILE} \
            >> $LOG 2>&1
        return 1
    else
        FILE_SIZE=$(($(stat -f %z ${OUTPUT_FILE}) / 1024))kb
        EXEC_TIME=$(( ($(get_time) - ${START_TIME}) / 1000 / 1000 ))
        printf "%10s" "${EXEC_TIME}ms"
        printf "    %8s | %8sms | %s\n" ${FILE_SIZE} ${EXEC_TIME} ${OUTPUT_FILE} \
            >> $LOG 2>&1
        return 0
    fi
}

# READ LISTS

FORMATS=()
while read FORMAT; do
    if ! [[ $FORMAT == '#'* ]]; then
        FORMATS+=("$FORMAT")
    fi
done < list-file-formats
echo FORMATS ${FORMATS[@]}

while read VIDEO; do
    if ! [[ $VIDEO == '#'* ]]; then
        VIDEOS+=("$VIDEO")
    fi
done < list-video-codecs
echo VIDEOS ${VIDEOS[@]}

while read AUDIO; do
    if ! [[ $AUDIO == '#'* ]]; then
        AUDIOS+=("$AUDIO")
    fi
done < list-audio-codecs
echo AUDIOS ${AUDIOS[@]}

# FOR ALL FORMATS
for (( i = 0; i < ${#FORMATS[@]}; i++ )); do
    echo "${FORMATS[i]}"
    # FOR ALL VIDEO CODECS
    for (( j = 0; j < ${#VIDEOS[@]}; j++ )); do
        # PRINT COLUMN LABLES
        if [[ j -eq 0 ]]; then
            printf "%10s" ""
            for (( k = 0; k < ${#AUDIOS[@]}; k++ )); do
                printf "%10s" $(acodec_to_filename "${AUDIOS[k]}")
            done
            printf "\n"
            printf "%10s" ""
            for (( k = 0; k < ${#AUDIOS[@]}; k++ )); do
                printf "%10s" '--------'
            done
            printf "\n"
        fi
        # PRINT ROW LABEL
        printf "%10s" "${VIDEOS[j]} |"
        # FOR ALL AUDIO CODECS
        for (( k = 0; k < ${#AUDIOS[@]}; k++ )); do
            ls . > /dev/null
            (create_video "${FORMATS[i]}" "${VIDEOS[j]}" "${AUDIOS[k]}")
        done
        printf "\n"
    done
done